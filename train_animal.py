
# coding: utf-8

# In[1]:

from __future__ import division, print_function, absolute_import

# Import tflearn and some helpers
import tflearn
import os
from tflearn.data_utils import shuffle
from tflearn.data_utils import image_preloader
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation

# Load path/class_id image file:
dataset_file = 'dataset.txt'
dataset_file_test = 'dataset_test.txt'

# Build the preloader array, resize images to 128x128
print("Loading dataset images...")
X, Y = image_preloader(dataset_file, image_shape=(32, 32, 3), mode='file', categorical_labels=True,   normalize=False, filter_channel=True)
print("Dataset images are loaded.")
print("Loading dataset test images...")
X_test, Y_test = image_preloader(dataset_file_test, image_shape=(32, 32, 3), mode='file', categorical_labels=True,   normalize=False, filter_channel=True)
print("Dataset test images are loaded.")

# Shuffle the data
X, Y = shuffle(X, Y)

# Make sure the data is normalized
img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()

# Create extra synthetic training data by flipping, rotating and blurring the
# images on our data set.
img_aug = ImageAugmentation()
img_aug.add_random_flip_leftright()
img_aug.add_random_rotation(max_angle=25.)
img_aug.add_random_blur(sigma_max=3.)


# In[4]:

# Define our network architecture:

# Input is a 32x32 image with 3 color channels (red, green and blue)
network = input_data(shape=[None, 32, 32, 3],
                     data_preprocessing=img_prep,
                     data_augmentation=img_aug)

# Step 1: Convolution
network = conv_2d(network, 32, 3, activation='relu')

# Step 2: Max pooling
network = max_pool_2d(network, 2)

# Step 3: Convolution again
network = conv_2d(network, 64, 3, activation='relu')

# Step 4: Convolution yet again
network = conv_2d(network, 64, 3, activation='relu')

# Step 5: Max pooling again
network = max_pool_2d(network, 2)

# Step 6: Fully-connected 512 node neural network
network = fully_connected(network, 512, activation='relu')

# Step 7: Dropout - throw away some data randomly during training to prevent over-fitting
network = dropout(network, 0.5)

# Step 8: Fully-connected neural network with two outputs (0=isn't a bird, 1=is a bird) to make the final prediction
network = fully_connected(network, 2, activation='softmax')

# Tell tflearn how we want to train the network
network = regression(network, optimizer='adam',
                     loss='categorical_crossentropy',
                     learning_rate=0.001)


# In[6]:

# Wrap the network in a model object
model = tflearn.DNN(network, tensorboard_verbose=0, checkpoint_path='./trash/dog-classifier.tfl.ckpt')

# Train it! We'll do 5 training passes and monitor it as it goes.
model.fit(X, Y, n_epoch=100, shuffle=True, validation_set=(X_test, Y_test),
          show_metric=True, batch_size=96,
          snapshot_epoch=True,
          run_id='dog-classifier')

# Save model when training is complete to a file
model.save("dog-classifier.tfl")
print("Network trained and saved as dog-classifier.tfl!")
os.system("python -u stat.py")
# In[ ]:
