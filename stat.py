# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import tflearn
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
from tflearn.data_utils import image_preloader
import scipy
import numpy as np
import argparse


dataset_file_test = 'dataset_test.txt'

print("Loading dataset test images...")
X_test, Y_test = image_preloader(dataset_file_test, image_shape=(32, 32, 3), mode='file', categorical_labels=True,   normalize=False, filter_channel=True)
print("Dataset test images are loaded.")

# Same network definition as before
img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()
img_aug = ImageAugmentation()
img_aug.add_random_flip_leftright()
img_aug.add_random_rotation(max_angle=25.)
img_aug.add_random_blur(sigma_max=3.)

network = input_data(shape=[None, 32, 32, 3],
                     data_preprocessing=img_prep,
                     data_augmentation=img_aug)
network = conv_2d(network, 32, 3, activation='relu')
network = max_pool_2d(network, 2)
network = conv_2d(network, 64, 3, activation='relu')
network = conv_2d(network, 64, 3, activation='relu')
network = max_pool_2d(network, 2)
network = fully_connected(network, 512, activation='relu')
network = dropout(network, 0.5)
network = fully_connected(network, 2, activation='softmax')
network = regression(network, optimizer='adam',
                     loss='categorical_crossentropy',
                     learning_rate=0.001)

model = tflearn.DNN(network, tensorboard_verbose=0, checkpoint_path='./trash/dog-classifier.tfl.ckpt')
model.load("dog-classifier.tfl")
print("Classifier is loaded.")


Y_predict = []
Y_reel = []
nb_reel_chien = 0
nb_reel_NONchien = 0

nb_predict_chien = 0
nb_predict_NONchien = 0

vrai_chien = 0
vrai_NONchien = 0
faux_chien = 0
faux_NONchien = 0

print("Calculation of statistics is in progress...")
for classe_reelle in Y_test:
    if (classe_reelle[0] == 1.0):
        nb_reel_chien = nb_reel_chien + 1
        Y_reel.append(0)
    else:
        nb_reel_NONchien = nb_reel_NONchien + 1
        Y_reel.append(1)


for image in X_test:
    # Predict
    prediction = model.predict([image])

    # Check the result.
    is_dog = np.argmax(prediction[0]) == 0

    if is_dog:
        Y_predict.append(0)
        nb_predict_chien = nb_predict_chien + 1
    else:
        Y_predict.append(1)
        nb_predict_NONchien = nb_predict_NONchien + 1

for (i, classe_reelle) in enumerate(Y_reel):
    if (Y_reel[i] == Y_predict[i]):
        if (Y_reel[i] == 0):
            vrai_chien = vrai_chien + 1
        else:
            vrai_NONchien = vrai_NONchien + 1
    else:
        if (Y_reel[i] == 0):
            faux_chien = faux_chien + 1
        else:
            faux_NONchien = faux_NONchien + 1

tx_class = (vrai_chien+vrai_NONchien) / (vrai_chien+faux_chien+vrai_NONchien+faux_NONchien)
rappel = vrai_chien / (vrai_chien+faux_NONchien)
precision = vrai_chien / (vrai_chien+faux_chien)

print()
print('nb_reel_chien:')
print(nb_reel_chien)
print('nb_reel_NONchien:')
print(nb_reel_NONchien)
print('nb_predict_chien:')
print(nb_predict_chien)
print('nb_predict_NONchien:')
print(nb_predict_NONchien)
print('vrai_chien:')
print(vrai_chien)
print('vrai_NONchien:')
print(vrai_NONchien)
print('faux_chien:')
print(faux_chien)
print('faux_NONchien:')
print(faux_NONchien)
print()
print('tx_class:')
print(tx_class)
print('rappel:')
print(rappel)
print('precision:')
print(precision)
