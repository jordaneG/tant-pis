#### Dépendances

- Tensorflow (https://www.tensorflow.org/get_started/os_setup)
- tflearn (http://tflearn.org/installation/)

Perso pour ne pas galérer, j'utilise Jupyter (http://jupyter.org/install.html) que j'ai installé avec Anaconda comme ça j'ai un environnement de dev où j'ai tout sans pourir mon pc.

Oubliez pas de ddl le jeu de données pour l'apprentissage. En attendant de réussir à balancer nos données persos. (https://s3-us-west-2.amazonaws.com/ml-is-fun/data.zip)

### Pour entrainer le réseau

`python train.py`

### Pour tester une image

`python r-u-a-bird.py dog.jpeg`

### Liens utiles

https://medium.com/@ageitgey/machine-learning-is-fun-part-3-deep-learning-and-convolutional-neural-networks-f40359318721#.cefb6uhsm